package com.ibm.spring.librarian.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import com.ibm.spring.librarian.dao.BooksDAO;
import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;

@Repository
@Primary
public class JpaBooksDAO implements BooksDAO{

	@PersistenceContext(name="librarian")
	EntityManager em;
	
	@Override
	public List<Publisher> getAllPublishers() { // JPQL -> HQL -> SQL
		return em.createQuery("select p from Publisher p").getResultList();
	}

	@Override
	public Publisher getPublisherById(Long id) {
		return em.find(Publisher.class, id);
	}

	@Override
	public List<Book> getBooksByPublisher(Publisher p) {
		return em.
				createQuery("select b from Book b where b.publisher=:p").
				setParameter("p", p).
				getResultList();
	}

	@Override
	public Book getBookById(Long mId) {
		return em.find(Book.class, mId);
	}

	@Override
	public Publisher addPublisher(Publisher p) {
		throw new IllegalArgumentException("fake exception");
	}

	@Override
	public Book addBook(Book b) {
		em.persist(b);
		return b;
	}
	
}
