package com.ibm.spring.librarian.dao.impl;

import com.ibm.spring.librarian.dao.BooksDAO;
import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;


import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class InMemoryBooksDAO implements BooksDAO {

    private Logger logger = Logger.getLogger(InMemoryBooksDAO.class.getName());

    private static List<Publisher> publishers = new ArrayList<>();

    private static List<Book> books = new ArrayList<>();

    static {

        Publisher p = new Publisher(1L, "O'Reilly",
                "http://2016.worldiaday.org/sites/default/files/oreilly.png");
        publishers.add(p);
        books.add(new Book(1L, "Learning Android", "Marko Gargenta", "http://akamaicovers.oreilly.com/images/0636920010883/cat.gif", 49, p));
        books.add(new Book(2L, "Getting started with SQL", "Thomas Nield", "http://akamaicovers.oreilly.com/images/0636920044994/cat.gif", 34, p));
        books.add(new Book(3L, "Learning Android", "Marko Gargenta", "http://akamaicovers.oreilly.com/images/0636920010883/cat.gif", 49, p));

        p = new Publisher(2L, "Microsoft Press",
                "https://upload.wikimedia.org/wikipedia/commons/4/4a/Microsoft_Press_Logo.svg");
        publishers.add(p);
        books.add(new Book(4L, "Programming for the Internet of Things: Using Windows 10 IoT Core and Azure IoT Suite", "Dawid Borycki", "https://i5.walmartimages.com/asr/6c8411bf-5615-429c-b9ab-3ee8171bafc6_1.f9dd41ce5641a78c2ae3b0b7240f83d7.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF", 49, p));
        books.add(new Book(5L, "Adaptive Code, Second Edition", "Gary McLean Hall", "https://target.scene7.com/is/image/Target/51710622?wid=520&hei=520&fmt=pjpeg", 19, p));

        p = new Publisher(3L, "MIT",
                "https://mitpress.mit.edu/sites/all/themes/mitclean/logo.png");
        publishers.add(p);
        books.add(new Book(6L, "Deep Learning", "Ian Goodfellow, Yoshua Bengio and Aaron Courville", "https://mitpress.mit.edu/sites/default/files/imagecache/booklist_node/9780262035613_0.jpg", 19, p));
        books.add(new Book(7L, "Shape, Talking about Seeing and Doing", "George Stiny", "https://mitpress.mit.edu/sites/default/files/imagecache/booklist_default/9780262693677.jpg", 21, p));
        books.add(new Book(8L, "The Stuff of bits", "Paul Dourish", "https://mitpress.mit.edu/sites/default/files/imagecache/booklist_node/9780262036207.jpg", 24, p));

    }


    public List<Publisher> getAllPublishers() {
        logger.info("fetching predefined list of publishers");
        return publishers;
    }

    public Publisher getPublisherById(Long id) {
        return publishers.stream().filter(p->p.getId()==id).findFirst().get();
    }

    public List<Book> getBooksByPublisher(Publisher p) {
        return books.stream().filter(b->b.getPublisher().getId()==p.getId()).collect(Collectors.toList());
    }

    public Book getBookById(Long bId) {
        return books.stream().filter(b->b.getId()==bId).findFirst().get();
    }

    public Publisher addPublisher(Publisher p) {
        final Long[] id = new Long[1];
        publishers.stream().max((p1,p2)->(int)(p1.getId()-p2.getId())).ifPresent(p1-> id[0] =p1.getId()+1);

        p.setId(id[0]);
        publishers.add(p);
        return p;
    }

    public Book addBook(Book b) {
        final Long[] id = new Long[1];
        books.stream().max((b1,b2)->(int)(b1.getId()-b2.getId())).ifPresent(b1-> id[0]=b1.getId()+1);

        b.setId(id[0]);
        books.add(b);
        return b;
    }

}
