package com.ibm.spring.librarian.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ibm.spring.librarian.model.Publisher;

public class PublisherMapper implements RowMapper<Publisher> {

	@Override
	public Publisher mapRow(ResultSet rs, int rowNum) throws SQLException {
		Publisher p = new Publisher();
		p.setId(rs.getLong("publisher_id"));
		p.setName(rs.getString("publisher_name"));
		p.setLogoImage(rs.getString("publisher_logo"));
		return p;
	}

}
