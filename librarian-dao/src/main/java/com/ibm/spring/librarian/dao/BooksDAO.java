package com.ibm.spring.librarian.dao;

import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;

import java.util.List;


public interface BooksDAO{

    List<Publisher> getAllPublishers();

    Publisher getPublisherById(Long id);

    List<Book> getBooksByPublisher(Publisher r);

    Book getBookById(Long mId);

    Publisher addPublisher(Publisher r);

    Book addBook(Book m);

}
