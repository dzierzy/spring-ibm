package com.ibm.spring.librarian.service.impl;

import com.ibm.spring.librarian.dao.BooksDAO;
import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;
import com.ibm.spring.librarian.service.api.BrowsingService;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Service
public class SimpleLibrarianService implements BrowsingService {

    Logger logger = Logger.getLogger(SimpleLibrarianService.class.getName());

    @Autowired
    private BooksDAO bDao;
    
	/*
	 * @Autowired PlatformTransactionManager tm;
	 */
    public List<Publisher> getPublishers() {
        return bDao.getAllPublishers();
    }

    public Publisher getPublisher(Long id){
        return bDao.getPublisherById(id);
    }



    public List<Book> getBooksForPublisher(Long rId) {
        Publisher r = getPublisher(rId);
        return bDao.getBooksByPublisher(r);
    }

    public Book getBookById(Long mId) {
        return bDao.getBookById(mId);
    }

    public Publisher addPublisher(Publisher p) {
        throw new UnsupportedOperationException("not implemented here");
    }




    @Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('REGULAR')")
    public Book addBook(Publisher p, Book b) {

    	//TransactionStatus ts = tm.getTransaction(new DefaultTransactionDefinition());
    	
        try {
            b.setPublisher(bDao.getPublisherById(p.getId()));

            b = bDao.addBook(b);
            if(b.getPrice()==0) {
                bDao.addPublisher(new Publisher());
                // throws exception
            }
            
            //tm.commit(ts);

        }catch (RuntimeException e){
        	//tm.rollback(ts);
            throw e;
        }
        return b;
    }

}
