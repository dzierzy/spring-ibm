package com.ibm.spring.librarian.service.api;

import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;

import java.util.List;


public interface BrowsingService {

    List<Publisher> getPublishers();

    public Publisher getPublisher(Long id);

    List<Book> getBooksForPublisher(Long rId);

    Book getBookById(Long mId);

    Publisher addPublisher(Publisher r);

    Book addBook(Publisher r, Book m);
}
