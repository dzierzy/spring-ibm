package com.ibm.spring.librarian.service.api;

public interface TipOfADayService {

	String getNextTip();
}
