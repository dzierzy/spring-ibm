package com.ibm.spring.librarian.service.api;

import com.ibm.spring.librarian.model.Book;

import java.util.Set;


public interface OrderService {

    void add(Book m);

    void remove(Book m);

    Set<Book> getOrderedMeals();

    void doit();

    void clear();

}
