package com.ibm.spring.librarian.service.impl;

import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ibm.spring.librarian.service.api.TipOfADayService;

@Service
public class TipServiceImpl implements TipOfADayService {

	@Resource
	List<String> tips;
	
	@Override
	public String getNextTip() {
		return tips.get(new Random().nextInt(tips.size()));
	}

}
