package com.ibm.spring.librarian.service.impl;



import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Order;
import com.ibm.spring.librarian.service.api.OrderService;


import java.io.Serializable;
import java.util.Set;
import java.util.logging.Logger;




public class SessionOrderService implements OrderService, Serializable {

    Logger logger = Logger.getLogger(SessionOrderService.class.getName());

    private Order o;

    public void add(Book m) {
        o.addMeal(m);
    }

    public void remove(Book m) {
        o.removeMeal(m);
    }

    public Set<Book> getOrderedMeals() {
        return o.getBooks();
    }


    public void doit() {
       logger.info("order confirmation: " + o);
       o.reset();
    }

    public void clear() {
       o.reset();
    }
}
