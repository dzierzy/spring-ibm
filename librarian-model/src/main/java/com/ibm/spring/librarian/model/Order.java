package com.ibm.spring.librarian.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;




public class Order implements Serializable {

    private Long id;

    private Set<Book> books = new HashSet<Book>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public void addMeal(Book m) {
        books.add(m);
    }

    public void removeMeal(Book m) {
        books.remove(m);
    }

    public void reset() {
        books.clear();
    }

    @Override
    public String toString() {
        return "Order{" +
                "books=" + books +
                '}';
    }
}
