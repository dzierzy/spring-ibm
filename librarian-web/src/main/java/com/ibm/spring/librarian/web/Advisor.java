package com.ibm.spring.librarian.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.ibm.spring.librarian.service.api.TipOfADayService;

@ControllerAdvice /* (annotations=AdvisorFilter.class) */
public class Advisor {

	@Autowired
	TipOfADayService tipService;
	
	@ModelAttribute
	public void addTip(Model model) {
		model.addAttribute("tip", tipService.getNextTip());
	}
	
}
