package com.ibm.spring.librarian.web;

import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.ibm.spring.librarian.model.Publisher;
import com.ibm.spring.librarian.service.api.BrowsingService;

@Controller
@AdvisorFilter
public class PublisherController {

	private Logger logger = Logger.getLogger(PublisherController.class.getName());
	
	@Autowired
	private BrowsingService bs;	
	
	@Autowired
	private MessageSource messageSource;
	
	
	//@RequestMapping(path="/publishers", method=RequestMethod.GET)
	@GetMapping("/publishers")
	public String getPublishers(Model model, Locale locale) {
		logger.info("about to fetch publishers list");
		
		List<Publisher> publishers = bs.getPublishers();		
		model.addAttribute("publishers", publishers);
		model.addAttribute("title", 
				messageSource.getMessage(
						"publisher.list", 
						new Object[] {}, 
						locale));
		
		return "publishers";
	}
	
	
	
}
