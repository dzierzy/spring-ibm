package com.ibm.spring.librarian.web;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ibm.spring.librarian.model.Book;

@Component
public class BookValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Book.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Book book = (Book) target;
		
		if(book.getTitle()==null || book.getTitle().trim().isEmpty()) {
			errors.rejectValue("title", "book.title.empty");
		}
		
		if(book.getPrice()<0) {
			errors.rejectValue("price", "book.price.negative");
		}
	}

}
