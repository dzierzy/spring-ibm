package com.ibm.spring.librarian.web;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	
	@Bean
	public List<String> tips(){
		return Arrays.asList("to czyta nie bladzi", "Ucz sie ucz...", "burn after reading");
	}

}
