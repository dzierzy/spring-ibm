package com.ibm.spring.librarian.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;
import com.ibm.spring.librarian.service.api.BrowsingService;

@RestController
@RequestMapping("/rest") //webapi
public class PublisherREST {
	
	@Autowired
	BrowsingService bs;
	
	@GetMapping("/publishers")
	public List<Publisher> getPublishers(){
		return bs.getPublishers();
	}
	
	@PostMapping("/publishers/{publisherId}/books")
	public ResponseEntity<Book> addBookForPublisher(
			@PathVariable("publisherId") long publisherId, 
			@RequestBody Book b/*
								 * , HttpServletResponse resp
								 */			
			) {
				
		b = bs.addBook(bs.getPublisher(publisherId), b);
		
		/*
		 * try { resp.setStatus(201); resp.flushBuffer(); } catch (IOException e) {
		 * e.printStackTrace(); }
		 */
		
		return ResponseEntity.status(HttpStatus.CREATED).body(b);
	}

}
