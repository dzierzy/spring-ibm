package com.ibm.spring.librarian;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ibm.spring.librarian.service.api.BrowsingService;

@Configuration
@ComponentScan("com.ibm.spring.librarian")
public class Launcher {

    public static void main(String[] args) {
        System.out.println("Launcher.main");

        ApplicationContext context = new AnnotationConfigApplicationContext(Launcher.class);
        
        BrowsingService bs = context.getBean(BrowsingService.class);

        bs.getPublishers().forEach(p-> System.out.println("publisher: " + p));
    }
}
