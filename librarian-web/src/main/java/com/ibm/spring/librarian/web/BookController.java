package com.ibm.spring.librarian.web;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.ibm.spring.librarian.model.Book;
import com.ibm.spring.librarian.model.Publisher;
import com.ibm.spring.librarian.service.api.BrowsingService;

@Controller
@RequestMapping("/books")
@SessionAttributes("publisherContext")
public class BookController {

	Logger logger = Logger.getLogger(BookController.class.getName());
	
	@Autowired
	private BrowsingService bs;
	
	@Autowired
	BookValidator validator;
	
	@InitBinder("book")
	void initBinding(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	
	@GetMapping
	public String getBooks(
			@RequestParam("publisherId") long publisherId, 
			Model model,
			@RequestHeader("User-Agent") String userAgent,
			@CookieValue(name="JSESSIONID", required=false) String sessionId
			) {
		
		logger.info("fetching books of publisher " + publisherId);
		logger.info("user agent: " + userAgent);
		logger.info("session id: " + sessionId);
		
		Publisher p = bs.getPublisher(publisherId);
		
		List<Book> books = bs.getBooksForPublisher(publisherId);
		model.addAttribute("books", books);
		model.addAttribute("title", "Books of " + p.getName());
		model.addAttribute("publisher", p);
		
		return "books";				
	}
	
	@GetMapping("/add")
	public String prepareAddBook(@RequestParam("publisherId") long publisherId, Model model) {
		
		logger.info("preparing add book action for publisher " + publisherId);
		
		Book book = new Book();
		book.setTitle("undefined");
		model.addAttribute("book", book);
		
		Publisher p = bs.getPublisher(publisherId);
		model.addAttribute("publisherContext", p);
		
		return "addBook";		
	}
	
	@PostMapping("/add")
	public String addBook(
			@Validated @ModelAttribute("book") Book book, 
			BindingResult br,
			@SessionAttribute("publisherContext") Publisher p) {
		logger.info("adding book " + book);
		
		
		if(br.hasErrors()) {
			return "addBook";
		}
		
		
		bs.addBook(p, book);
		
		return "redirect:/books?publisherId=" + p.getId();
	}
	
	
}
