<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="./header.jsp" flush="true"/>

		<form:form action="add" method="post" modelAttribute="book">
            <table>
                <tr>
                	<td>
                		Title: <form:input path="title"/>
                		<br/>
                		<form:errors path="title"/>
                	</td>                
                </tr>  
                <tr>
                	<td>
                		Author: <form:input path="author"/>
                	</td>                
                </tr>       
                <tr>
                	<td>
                		Cover: <form:input path="cover"/>
                	</td>                
                </tr>  
                <tr>
                	<td>
                		Price: <form:input path="price"/>
                		<br/>
                		<form:errors path="price"/>
                	</td>                
                </tr> 
                <tr>
                	<td>
                		<input type="submit"/>
                	</td>                
                </tr>
            </table>
         </form:form>

<jsp:include page="./footer.jsp" flush="true"/>
