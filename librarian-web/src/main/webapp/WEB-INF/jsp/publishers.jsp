<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<jsp:include page="./header.jsp" flush="true"/>

            <table>
                <tr>
                    <th><spring:message code="publisher.logo"/></th>
                    <th><spring:message code="publisher.name"/></th>
                </tr>
                <c:forEach var="p" items="${publishers}">
                    <tr>
                        <td>
                            <img src="${p.logoImage}"/>
                        </td>
                        <td>
                            <a href="./books?publisherId=${p.id}">${p.name}</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>

<jsp:include page="./footer.jsp" flush="true"/>

