
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>


<header>
    <title>Librarian</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<c:url value = "/resources/css/style.css"/>"/>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="./resources/js/script.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>

</header>

<body>
<header>
    <a href="./"><h1>Librarian</h1></a>
    <h2>${title}</h2>
    <h3>        
    </h3>
</header>
<section>
