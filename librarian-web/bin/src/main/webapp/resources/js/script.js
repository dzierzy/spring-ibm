$(document).ready(function(){

    $("#emptyItems").click(function(){alert('clearing ...');resetOrder();})
    $("#submitItems").click(function(){alert('submitting ...');submitOrder();})
    getOrder();

});

var serverUri = window.location.origin + "/librarian-web";


function getOrder(){
     $.ajax({
          method: "GET",
          url: serverUri + "/orderitems",
          Accept : "application/json",
          contentType: "application/json"
     }).done(function(books, status){
        $("#orders table > tbody > tr").remove();
        var totalPrice = 0;
        books.forEach(function(book){
            $("#orders table")
                .append("<tr><td>" + book.name + "</td><td>" +
                 book.price + "</td><td>" + book.publisherName +
                 "</td><td onclick='javascript:removeFromOrder(" + book.id + ")'><p class='action'>-</p></td></tr>");
            totalPrice = totalPrice + book.price;
        });
        $("#totalPrice > span").text(totalPrice);
    } );
}

function addToOrder(id){
    $.ajax({
      method: "POST",
      url: serverUri + "/orderitems/"+id
    }).done(function() {
        getOrder();
    });
}

function removeFromOrder(id){
    $.ajax({
      method: "DELETE",
      url: serverUri + "/orderitems/" + id
    }).done(function() {
        getOrder();
    });
}

function resetOrder(){
     $.ajax({
       method: "DELETE",
       url: serverUri + "/orderitems"
     }).done(function() {
         getOrder();
     });
 }

 function submitOrder(){
     $.ajax({
       method: "PUT",
       url: serverUri + "/orderitems"
     }).done(function() {
         getOrder();
         alert("The order has been submited. Thank You!");
     });
 }